// Code your testbench here
// or browse Examples
interface usb_intf();
  logic x;
  logic y;
  logic z;
endinterface : usb_intf

module usb_rtl(input x, input y, input z);
  wire x,y,z;
endmodule : usb_rtl

class Driver;
   virtual usb_intf intf;
  
  function void drive();
    intf.x = 0;
    intf.y = 2;
    intf.z = 3;
  endfunction : drive
endclass : Driver

module top;
  usb_intf u_intf();
  
  usb_rtl u_rtl(u_intf.x, u_intf.y, u_intf.z);
  
  initial begin
    $dumpfile("dump.vcd");
    $dumpvars(0,top);
  end
  
  initial begin
    Driver d;
    d = new();
    d.intf = u_intf;
    d.drive();
  end
endmodule : top